﻿using System;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;

namespace Labs
{
    class XmlSerialization : ISerialization
    {
        Dictionary<string, XmlSerializer> Types = new Dictionary<string, XmlSerializer>();
        public string serialization<T>(T output)
        {
            var xmlSettings = new XmlWriterSettings();
            xmlSettings.OmitXmlDeclaration = true;
            var xmlSerializerNamespace = new XmlSerializerNamespaces();
            xmlSerializerNamespace.Add("", "");
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, xmlSettings))
            {
                //done: не создавать XmlSerializer на каждый вызов метода serialization, вызывать new только один раз для каждого T
                var typeoft = typeof(T);
                var typeofttostr = typeoft.ToString();
                if (Types.ContainsKey(typeofttostr))
                {
                    Types[typeofttostr].Serialize(writer, output, xmlSerializerNamespace);
                }
                else
                {
                    var newserializer = new XmlSerializer(typeoft); 
                    newserializer.Serialize(writer, output, xmlSerializerNamespace);
                    Types.Add(typeofttostr, newserializer);
                }
            }
            return stringWriter.ToString();
        }

        public T deserialization<T>(string obj)
        {
            var reader = XmlReader.Create(new MemoryStream(Encoding.UTF8.GetBytes(obj)));
            return (T)new XmlSerializer(typeof(Input)).Deserialize(reader);
        }
    }
}
