﻿namespace Labs
{
    interface IClient
    {
        bool Ping();
        string GetInputData();
        bool WriteAnswer(string serializedOutput);
    }
}
