﻿namespace Labs
{
    class ServerCalculator : IServerCalculator
    {
        private JsonSerialization serializer;
        private Calculator calc;
        private Input input;
        private Output output;
        public ServerCalculator(string body)
        {
            serializer = new JsonSerialization();
            input = serializer.deserialization<Input>(body);
            calc = new Calculator();
            output = calc.GenerateOutput(input);
        }
        public string calculateAnswer()
        {
            //done: сделать JsonSerialization, Calculator полями класса и присвоить их в конструткоре

            return (input == null) ? "" : serializer.serialization(output);
        }
    }
}
