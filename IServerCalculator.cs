﻿namespace Labs
{
    interface IServerCalculator
    {
        string calculateAnswer();
    }
}
