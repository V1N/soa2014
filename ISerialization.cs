﻿namespace Labs
{
    
    interface ISerialization
    {
        string serialization<T>(T output);
        T deserialization<T>(string input);
    }
}
