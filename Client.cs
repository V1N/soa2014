﻿using System.Text;
using System.Net;
using System.IO;

namespace Labs
{
    public static class RequestExtensions
    {
        public static void Write(this WebRequest request,string body)
        {
            using (var writer = request.GetRequestStream())
            {
                writer.Write(Encoding.UTF8.GetBytes(body), 0, (int)request.ContentLength);
            }
        }
    }

    //done: сделать базовый класс ClientBase, убрать тула метод getResponse
    abstract class ClientBase
    {
        protected string urlServer;
        protected string port;
        protected HttpWebResponse getResponse(string methodName, string body = "")
        {
            var request = WebRequest.Create(string.Format("{0}:{1}/{2}", urlServer, port, methodName));
            request.Timeout = 150;
            request.ContentLength = Encoding.UTF8.GetByteCount(body);
            request.Method = (body == string.Empty) ? "GET" : "POST";
            if (body.Length > 0)
            {
                request.Write(body);
                //done: написать extension метод для записи в поток

                /*using (var writer = request.GetRequestStream())
                {
                    writer.Write(Encoding.UTF8.GetBytes(body), 0, (int)request.ContentLength);
                }*/
            }

            try
            {
                return (HttpWebResponse)request.GetResponse();
            }
            catch
            {
                return null;
            }
        }
    }
    
    class Client : ClientBase, IClient 
    {
        public Client(string urlServer, string port)
        {
            this.urlServer = urlServer;
            this.port = port;
        }

        public bool WriteAnswer(string serializedOutput)
        {
            var response = (HttpWebResponse)getResponse("writeAnswer", serializedOutput);
            return response != null;
        }

        public string GetInputData()
        {
            var response = (HttpWebResponse)getResponse("getInputData");
            return (response != null) ? new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd() : string.Empty;
        }

        public bool Ping()
        {
            var response = (HttpWebResponse)getResponse("ping");
            return (response != null) ? response.StatusCode == HttpStatusCode.OK : false;
        }

        
    }
}
